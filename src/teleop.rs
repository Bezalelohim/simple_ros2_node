use std::{
    io::Error,
    env,
};
use console::Term;
use rclrs;
use geometry_msgs::msg::{Twist,Vector3};

const BURGER_MAX_LIN_VEL: f64 = 0.22;
const BURGER_MAX_ANG_VEL: f64 = 2.84;
const WAFFLE_MAX_LIN_VEL: f64 = 0.26;
const WAFFLE_MAX_ANG_VEL: f64 = 1.82;
const LIN_VEL_STEP_SIZE: f64 = 0.01;
const ANG_VEL_STEP_SIZE: f64 = 0.1;

fn print_vels(target_linear_velocity: f64, target_angular_velocity: f64) {
    println!(
        "currently:\tlinear velocity {:.2}\t angular velocity {:.2}",
        target_linear_velocity, target_angular_velocity
    );
}

fn make_simple_profile(output: f64, input: f64, slop: f64) -> f64 {
    if input > output {
        input.min(output + slop)
    } else if input < output {
        input.max(output - slop)
    } else {
        input
    }
}

fn constrain(input_vel: f64, low_bound: f64, high_bound: f64) -> f64 { 
    if input_vel < low_bound {
        low_bound
    } else if input_vel > high_bound {
        high_bound
    } else {
        input_vel
    }
}

fn check_linear_limit_velocity(robot_modle: &str,velocity: f64) -> f64 {
    match robot_modle {
        "burger" => constrain(velocity, -BURGER_MAX_LIN_VEL, BURGER_MAX_LIN_VEL),
        "waffle" => constrain(velocity, -WAFFLE_MAX_LIN_VEL, WAFFLE_MAX_LIN_VEL),
        "waffle_pi" => constrain(velocity, -WAFFLE_MAX_LIN_VEL, WAFFLE_MAX_LIN_VEL),
        _ => constrain(velocity, -BURGER_MAX_LIN_VEL, BURGER_MAX_LIN_VEL)
    }
}

fn check_angular_limit_velocity(robot_modle:&str, velocity: f64) -> f64 {
    match robot_modle {
        "burger" => constrain(velocity, -BURGER_MAX_ANG_VEL, BURGER_MAX_ANG_VEL),
        "waffle" => constrain(velocity, -WAFFLE_MAX_ANG_VEL, WAFFLE_MAX_ANG_VEL),
        "waffle_pi" => constrain(velocity, -WAFFLE_MAX_ANG_VEL, WAFFLE_MAX_ANG_VEL),
        _ => constrain(velocity, -WAFFLE_MAX_ANG_VEL, WAFFLE_MAX_ANG_VEL),
    }
}

fn command_prompt() -> Result<char,Error>{
    println!("Press any key to continue...");
    Ok(Term::stdout().read_char().expect("Could not read Char"))
}

fn main() {
    let model: &str = &env::var("TURTLEBOT3_MODEL").unwrap_or("burger".to_string());
    let node = rclrs::create_node(
        &rclrs::Context::new(env::args()).unwrap(),
        "teleop_keyboard").unwrap();
    let publisher = node.create_publisher::<Twist>("cmd_vel", rclrs::QOS_PROFILE_DEFAULT).unwrap();

    let mut target_linear_velocity = 0.0;
    let mut target_angular_velocity = 0.0;

    let mut control_linear_velocity = 0.0;
    let mut control_angular_velocity = 0.0;

    println!("Control Your TurtleBot3!");

    std::iter::repeat(()).for_each(|()| {

        match command_prompt().unwrap() {
            'w' => {
                target_linear_velocity = check_linear_limit_velocity(model,target_linear_velocity + LIN_VEL_STEP_SIZE);
                print_vels(target_linear_velocity, target_angular_velocity);
            }
            'x' => {
                target_linear_velocity =
                    check_linear_limit_velocity(model,target_linear_velocity - LIN_VEL_STEP_SIZE);
                print_vels(target_linear_velocity, target_angular_velocity);
            }
            'a' => {
                target_angular_velocity = check_angular_limit_velocity(model,target_angular_velocity + ANG_VEL_STEP_SIZE);
                print_vels(target_linear_velocity, target_angular_velocity);
            }
            'd' => {
                target_angular_velocity =check_angular_limit_velocity(model,target_angular_velocity - ANG_VEL_STEP_SIZE);
                print_vels(target_linear_velocity, target_angular_velocity);
            }
            ' ' => {
                target_linear_velocity = 0.0;
                control_linear_velocity = 0.0;
                target_angular_velocity = 0.0;
                control_angular_velocity = 0.0;
                print_vels(target_linear_velocity, target_angular_velocity);
            }
            's' =>{
                target_linear_velocity = 0.0;
                control_linear_velocity = 0.0;
                target_angular_velocity = 0.0;
                control_angular_velocity = 0.0;
                print_vels(target_linear_velocity, target_angular_velocity);
            }
            _ => {}
        }

        publisher.publish(&Twist{
            linear: Vector3{
                x: make_simple_profile(control_linear_velocity,target_linear_velocity,LIN_VEL_STEP_SIZE / 2.0),
                y: 0.0,
                z: 0.0,
            },
            angular : Vector3{
                x: 0.0,
                y: 0.0,
                z: make_simple_profile(control_angular_velocity,target_angular_velocity,ANG_VEL_STEP_SIZE / 2.0),
            }
        }).unwrap();
    });
}
