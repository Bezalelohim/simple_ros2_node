
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='simple_ros_rs',
            executable='simple_publisher',
            name='pub'
        ),
        Node(
            package='simple_ros_rs',
            executable='simple_subscriber',
            name='sub'
        ),
    ])
